# -*- coding: utf-8 -*-
import cv2
import os
import sys
class MyVideo(object):
    """Processing video by reading frames"""
    def __init__(self, video):
        super(MyVideo, self).__init__()
        self.video = video
        self.rgb_frames = []
        self.fps = 0
        self.cap = cv2.VideoCapture(self.video)
        
        if not os.path.isfile(self.video):
            print 'Video file not found. The system exited!'
            sys.exit()

    #----------------------------------------------------------------------
    def read_frames(self):
        """Reading frames of a video"""
        print 'Be starting reading video'
        while True:
            flag, rbg_frame = self.cap.read()
            #print 'reading'
            if flag == 0:
                break
            else:                
                self.rgb_frames.append(rbg_frame)
        cv2.destroyAllWindows()
        self.cap.release()        
        print 'Reading video has been finished!'        
        return self.rgb_frames

    #----------------------------------------------------------------------
    def convert_rgb_gray(self, rgb_frame):
        """Convert a rgb image to a gray image"""
        img_gray = cv2.cvtColor(rgb_frame, cv2.COLOR_BGR2GRAY)
        return img_gray        

    #----------------------------------------------------------------------
    def show_img_frame(self, winname, frame, hold_times = 1):
        """Show a frame as an image"""
        cv2.imshow(winname, frame)
        if cv2.waitKey(hold_times) & 0xFF == ord('q'):
            cv2.destroyWindow()    

    #----------------------------------------------------------------------
    def show_img_frame_from_list_frames(self, winname, frames, hold_times = 1):
        """Show a list of frames as a video timeslape"""
        for frame in frames:
            cv2.imshow(winname, frame)
            if cv2.waitKey(hold_times) & 0xFF == ord('q'):
                cv2.destroyWindow()        

    #----------------------------------------------------------------------
    def get_size(self, frame):
        """Get size of a frame"""
        try:        
            cv_size = tuple(frame.shape[1::-1])
            return cv_size
        except:
            pass
    #----------------------------------------------------------------------
    def resize(self, img, size):
        """Resize an image"""
        resized_img = cv2.resize(img, size, 0, 0, interpolation = cv2.INTER_LINEAR)
        return resized_img

    def get_fps(self):
        """Get FPS of the video"""        
        return self.cap.get(cv2.cv.CV_CAP_PROP_FPS)        
        