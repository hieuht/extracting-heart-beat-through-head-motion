# -*- coding: utf-8 -*-
import cv2
import os
import sys
import numpy as np
from matplotlib import pyplot as plt
cascade_filter_xml_file = "lbpcascade_frontalface.xml"
img = cv2.imread('/hcm/Media/Photos/AnhHoSo/IMG_9977 copy.jpg')
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
scale_factor= 1.2
min_neighbours=2
min_size=(15, 15) 
flag=cv2.cv.CV_HAAR_SCALE_IMAGE
face_cascade = cv2.CascadeClassifier(cascade_filter_xml_file)

region_face = face_cascade.detectMultiScale(img, scaleFactor=scale_factor,
                                                  minNeighbors=min_neighbours, minSize=min_size,
                                                  flags=flag)

face_x, face_y, face_width, face_height = region_face[0]
face_x = face_x+face_width/4
face_width = face_width/2
face_height = face_height/10*9
face = img[face_y:face_y+face_height, face_x:face_x+face_width, :]
gray_face = cv2.cvtColor(face,cv2.COLOR_BGR2GRAY)
img_up = img[face_y:face_y+face_height*10/40, face_x:face_x+face_width, :]
img_up_gray = cv2.cvtColor(img_up,cv2.COLOR_BGR2GRAY)

corners = cv2.goodFeaturesToTrack(gray_face, mask=None, maxCorners=1500,
                                  qualityLevel=0.005,
                                  minDistance=0.5)
#corners = np.int0(corners)

for i in corners:
    x,y = i.ravel()
    cv2.circle(face,(x,y),3,255,-1)
plt.imshow(face),plt.show()