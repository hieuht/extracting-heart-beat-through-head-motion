# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from sklearn import decomposition
########################################################################
class MyPCA:
    """"""

    #----------------------------------------------------------------------
    def __init__(self, data, n_components):
        """Constructor"""
        self.n_components = n_components
        self.pca_data = None
        self.data = np.asarray(data)
        
    def do_pca(self):        
        pca = decomposition.PCA(n_components= self.n_components)
        data = self.data.T
        pca.fit(data)
        X = pca.transform(data)
        X = X.T
        self.pca_data = X
    
    def save_pca_data_to_txt(self, file_name):
        print '-- Save PCA Data to file %s' % file_name                
        x = np.asarray(self.pca_data)
        np.savetxt(file_name, x)
        
    def save_pca_data_to_plot(self, plimit = 20):
        for i, point in enumerate(self.pca_data):            
            if i < plimit:
                print '---- Plot pca point ' + str(i)
                plt.plot(np.arange(0, len(point)) / 250.0, point)
                plt.xlabel('Time(s)')
                plt.ylabel('Y')            
                plt.savefig("plot/PCA/point-"+str(i)+".jpg")
                plt.close() 
        