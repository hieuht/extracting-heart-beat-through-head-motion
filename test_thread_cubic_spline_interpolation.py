import Queue
import threading
import time
import numpy as np
from scipy.interpolate import interp1d
import datetime
start_time = datetime.datetime.now()
exitFlag = 0
data = np.loadtxt("y_values")
points_after_cubic_spline = []
class myThread (threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
    def run(self):
        print "Starting " + self.name
        process_data(self.name, self.q)
        print "Exiting " + self.name

def process_data(threadName, q):
    while not exitFlag:
        queueLock.acquire()
        if not workQueue.empty():
            data = q.get()
            queueLock.release()
            #Processing
            print "%s processing %s" % (threadName, len(data))
            y_point_len = len(data)
            y_point_times = np.arange(y_point_len, dtype=float)
            f = interp1d(y_point_times, data, kind='cubic')
            y_point_new_times = np.arange(0, y_point_len-1, 0.05)
            y_point_new = f(y_point_new_times)
            points_after_cubic_spline.append(y_point_new)            
        else:
            queueLock.release()
        time.sleep(1)
        
        
threadList = ["Thread-1", "Thread-2", "Thread-3", "Thread-4",
              "Thread-5", "Thread-6", "Thread-7", "Thread-8"]
queueLock = threading.Lock()
workQueue = Queue.Queue()
threads = []
threadID = 1

# Create new threads
for tName in xrange(15):
    thread = myThread(threadID, "Thread-" + str(tName), workQueue)
    thread.start()
    threads.append(thread)
    threadID += 1

# Fill the queue
queueLock.acquire()
for i, word in enumerate(data):
    workQueue.put(word)
queueLock.release()

# Wait for queue to empty
while not workQueue.empty():
    pass

# Notify threads it's time to exit
exitFlag = 1

# Wait for all threads to complete
for t in threads:
    t.join()
print "Exiting Main Thread"
stop_time = datetime.datetime.now()

print stop_time - start_time
