# -*- coding: utf-8 -*-
import cv2
import os
import sys
import numpy as np
class MyFace(object):
    def __init__(self):
        super(MyFace, self).__init__()        
        self.cascade_filter_xml_file = "lbpcascade_frontalface.xml"
        self.lucas_kanade_params = dict(winSize=(40, 40), #Giá trị này là bằng 1/125 khu vực khuôn mặt xử lý
                                        maxLevel=5,
                                        criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 30, 0.005))

        if not os.path.isfile(self.cascade_filter_xml_file):
            print 'The cascade filter xml file not found. The system exited!'
            sys.exit()
        else:
            self.face_cascade = cv2.CascadeClassifier(self.cascade_filter_xml_file)

    #----------------------------------------------------------------------        
    def detect_region_faces(self, rgb_frame, scale_factor=1.2, 
                     min_neighbours=2, min_size=(15, 15), 
                     flag=cv2.cv.CV_HAAR_SCALE_IMAGE):
        """Base on cascade filter, the process will be detected some faces in a frame"""
        region_faces = self.face_cascade.detectMultiScale(rgb_frame, scaleFactor=scale_factor,
                                                          minNeighbors=min_neighbours, minSize=min_size,
                                                          flags=flag)

        return region_faces

    #----------------------------------------------------------------------
    def get_max_region_face(self, region_faces):
        """Get the largest face size in a frame"""
        _idFace = 0
        _faceArea = -1
        _i = 0
        for region_face in region_faces:
            _i += 1
            _x, _y, _w, _h = region_face
            _area = _w*_h
            if _faceArea < _area:
                _faceArea = _area
                _idFace = _i
        if _idFace > 0:
            region_face = region_faces[_idFace-1]
            return region_face
        else:
            return None       

    #----------------------------------------------------------------------
    def face_region_processing(self, rgb_frame, region_face):
        """The get region face for processing"""
        try:
            face_x, face_y, face_width, face_height = region_face
            face_x = face_x+face_width/4
            face_width = face_width/2
            face_height = face_height/10*9
            #cv2.putText(rgb_frame,'Region Processing', (face_x, face_y-5),
                        #cv2.FONT_HERSHEY_SIMPLEX, 0.35, (255, 255, 255), 1)            
            cv2.rectangle(rgb_frame, (face_x, face_y), 
                          (face_x+face_width, face_y+face_height * 10 / 40), 
                          (200, 100, 0), 1) # Remove Eyes, Selecting Up ROI
            #cv2.rectangle(rgb_frame, (face_x, (face_y + face_height / 20 * 11)), 
                          #(face_x+face_width, (face_y + face_height / 20 * 11) + face_height * 10 / 30), 
                          #(200, 500, 0), 3) # Remove Eyes, Selecting Up ROI
            #tmp = (face_y + face_height / 20 * 11) + face_height * 10 / 30
            img_up = rgb_frame[face_y:face_y+face_height*10/40, face_x:face_x+face_width, :]
            #img_down = rgb_frame[face_y:tmp, face_x:face_x+face_width, :]
            #face = rgb_frame[face_y:face_y+face_height, face_x:face_x+face_width, :]

            return img_up
        except:
            pass

    #----------------------------------------------------------------------
    def get_feature_points(self, gray_frame, max_corners, quality_level, min_distance):
        """Getting feature poins of a gray frame"""
        points = cv2.goodFeaturesToTrack(gray_frame, mask=None, maxCorners=max_corners,
                                         qualityLevel=quality_level,
                                         minDistance=min_distance)
        return points

    #----------------------------------------------------------------------
    def lucas_kanade_optical_flow(self, prev_gray_frame, next_gray_frame, 
                                  prev_points):
        """Tracking features points base on Lucas Kanade Optical Flow algorithm"""
        new_poins, status, error = cv2.calcOpticalFlowPyrLK(prev_gray_frame, next_gray_frame,
                                                            prev_points, None, **self.lucas_kanade_params)
        return new_poins, status, error

    #----------------------------------------------------------------------
    def random_color(self, n):
        """Random color N points """
        color = np.random.randint(0, 255, (n, 3))
        return color
        
    #----------------------------------------------------------------------
    def create_mask(self, frame):
        """Create a mask for a frame"""
        mask = np.zeros_like(frame)
        return mask
        