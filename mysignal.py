# -*- coding: utf-8 -*-
from scipy.fftpack import rfft, irfft
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import freqz
import scipy.signal as signal

class MySignal:
    """"""
    #----------------------------------------------------------------------
    def __init__(self, selected_feature_points):
        """Constructor"""
        self.Fs = 250.0
        self.nyq = 0.5 * self.Fs        
        self.lowcut = 0.75 / self.nyq
        self.highcut = 5.0 / self.nyq
        self.order = 5
        self.selected_feature_points = np.array(selected_feature_points)
        #self.selected_feature_points = self.selected_feature_points.astype(np.float) #...of floats        
        self.selected_feature_pointsf = []
    
    def butter_bandpass_filter(self, data):
        #order = 2
        #BPM_L = 40;    #% Heart rate lower limit [bpm]
        #BPM_H = 230;   #% Heart rate higher limit [bpm]
        #Wn = [((BPM_L / 60) / self.Fs * 2), ((BPM_H / 60) / self.Fs * 2)]
        #print Wn
        order = 5
        Wn = [self.lowcut, self.highcut] # Cutoff bandpass frequency
        B, A = signal.butter(order, Wn, output='ba', btype = 'bandpass')
        dataf = signal.filtfilt(B, A, data)
        return dataf
        
    def bandpass_filter(self):
        for data in self.selected_feature_points:
            new_point_filter = self.butter_bandpass_filter(data)
            self.selected_feature_pointsf.append(new_point_filter)
    #----------------------------------------------------------------------                
    def save_good_data_to_text(self, file_name):
        """"""
        tmp = np.asarray(self.selected_feature_pointsf)
        np.savetxt(file_name, tmp)
        
    #----------------------------------------------------------------------
    def save_plot(self, plimit = 20):
        i = 0        
        for point, pointf in zip(self.selected_feature_points, self.selected_feature_pointsf):
            if i < plimit:                
                print '---- Saving Plot Point ' + str(i)            
                x = np.arange(0, len(point)) / 250.0
                fig = plt.figure()
                ax1 = fig.add_subplot(211)
                plt.plot(x,point, 'b-')
                plt.plot(x,pointf, 'r-',linewidth=2)
                plt.ylabel("Y")
                plt.legend(['Original','Filtered'])
                plt.title("5th order butterworth bandpass filter[0.75-5]Hz")
                plt.savefig("plot/BandPassFilter/point-"+str(i)+".jpg")
                plt.close()
                i += 1