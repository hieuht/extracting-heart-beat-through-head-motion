import numpy as np

data = np.loadtxt("phase4_temporal_filtering")
from sklearn import decomposition
pca = decomposition.PCA(n_components=5)
data = data.T
pca.fit(data)
X = pca.transform(data)
X = X.T
print X.shape
np.savetxt("phase5_pca_data", X)