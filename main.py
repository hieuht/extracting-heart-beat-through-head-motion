# -*- coding: utf-8 -*-
import face
import video
import processing
import mysignal
import mypca
import mypeak
import matplotlib.pyplot as plt
import numpy as np
import cv2
import datetime
from scipy.fftpack import rfft
##########################################
# Define for getting feature points
file_video = "video/3.Internet.mp4"
max_corners = 1500
#quality_level = 0.05
quality_level = 0.04
min_distance = 0.5
##########################################
# Define Class
v = video.MyVideo(file_video)
vfps = v.get_fps()
print 'FPS of the video %s' % str(vfps)
f = face.MyFace()
rgb_frames = v.read_frames()
total_frames = len(rgb_frames)    
first_frame = rgb_frames[0]

##########################################
def get_face_processing(rgb_frame, f, v):
    region_faces = f.detect_region_faces(rgb_frame)
    if len(region_faces) > 0:
        max_region_face = f.get_max_region_face(region_faces)        
        prevRgbFaceRegionProcessing = f.face_region_processing(rgb_frame, max_region_face)
        prevGrayFaceRegionProcessing = v.convert_rgb_gray(prevRgbFaceRegionProcessing)
        return prevRgbFaceRegionProcessing, prevGrayFaceRegionProcessing
    return None, None

def get_good_feature_points():
    _tmp_dict = {}
    for rgb_frame in rgb_frames[:1]:        
        prevRgbFaceRegionProcessing, prevGrayFaceRegionProcessing, prevGrayRegionProcessingSize = phase1_detecting_region_face_processing(rgb_frame, f, v)
        if prevRgbFaceRegionProcessing != None and prevGrayFaceRegionProcessing != None and prevGrayRegionProcessingSize != None:
            prevFeaturePoints = phase2_detecting_feature_points(f, max_corners, prevGrayFaceRegionProcessing, quality_level, min_distance)
            _tmp_dict.update({len(prevFeaturePoints): prevFeaturePoints})
    max_key = max(_tmp_dict.keys())
    prevFeaturePoints = _tmp_dict[max_key]
    return prevGrayRegionProcessingSize, prevGrayFaceRegionProcessing, prevFeaturePoints

def phase1_detecting_region_face_processing(rgb_frame, f, v):
    prevRgbFaceRegionProcessing, prevGrayFaceRegionProcessing = get_face_processing(rgb_frame, f, v)
    prevGrayRegionProcessingSize = v.get_size(prevGrayFaceRegionProcessing)
    #v.show_img_frame("Waiting for Detecting Face and Its Feature Points", first_frame, hold_times= 10)
    return prevRgbFaceRegionProcessing, prevGrayFaceRegionProcessing, prevGrayRegionProcessingSize
    
def phase2_detecting_feature_points(f, max_corners, prevGrayFaceRegionProcessing, quality_level, min_distance):
    try:    
        prevFeaturePoints = f.get_feature_points(prevGrayFaceRegionProcessing, max_corners, 
                                                 quality_level, 
                                                 min_distance)
        return prevFeaturePoints        
    except:
        pass
    
def phase3_tracking_feature_points(prevGrayRegionProcessingSize, prevGrayFaceRegionProcessing, prevFeaturePoints):
    featureFrameProcessing = [] # Luu tru thong tin cac feature point thay doi vi tri sau cac frame    
    frame_id = 0
    for rgb_frame in rgb_frames:            
        nextRgbFaceRegionProcessing, nextGrayFaceRegionProcessing = get_face_processing(rgb_frame, f, v)
        if nextRgbFaceRegionProcessing != None or  nextGrayFaceRegionProcessing != None:            
            mask = f.create_mask(nextRgbFaceRegionProcessing)
            try:
                nextGrayFaceRegionProcessing = v.resize(nextGrayFaceRegionProcessing, prevGrayRegionProcessingSize)
                Points, st, err = f.lucas_kanade_optical_flow(prevGrayFaceRegionProcessing, nextGrayFaceRegionProcessing, 
                                                              prevFeaturePoints)
                if st[1] == 1:
                    newGoodFeaturePoints = Points[st == 1]
                    print '-- Tracked %s Feature Point in frame %s/%s' % (len(newGoodFeaturePoints), frame_id, total_frames)
                    featureFrameProcessing.append(newGoodFeaturePoints)
                    #Cap nhap lai frame va feature point cua no o phase 1 bang frame hien tai.
                    prevGrayFaceRegionProcessing = nextGrayFaceRegionProcessing.copy()
                    prevFeaturePoints = newGoodFeaturePoints.reshape(-1, 1, 2)            
            except cv2.error as e:
                pass
        frame_id += 1      
    print '-- Total Processed Frame', frame_id
    return featureFrameProcessing

def phase4_temporal_filtering(data):
    print 'Phase 4: Temporal Filtering'
    s = mysignal.MySignal(data)
    s.bandpass_filter()
    s.save_good_data_to_text("raw/phase4_temporal_filtering")
    s.save_plot(plimit = 0)
    return s.selected_feature_pointsf
    

def phase5_pca_data(fft_good_data):
    print 'Phase 5: PCA Data'    
    mp = mypca.MyPCA(fft_good_data, n_components = 5)
    mp.do_pca()
    mp.save_pca_data_to_txt("raw/phase5_pca_data")
    mp.save_pca_data_to_plot(plimit = 0)
    return mp.pca_data
##########################################
# Main Program
def main():
    start_time = datetime.datetime.now()    
    # Doing phases
    print "Phase 1 & 2: Detecting region face and its feature points"        
    prevGrayRegionProcessingSize, prevGrayFaceRegionProcessing, prevFeaturePoints = get_good_feature_points()        
    print '-- Determine Feature Points: ', len(prevFeaturePoints)    
    print 'Phase 3: Processing Tracked Points'    
    featureFrameProcessing = phase3_tracking_feature_points(prevGrayRegionProcessingSize, prevGrayFaceRegionProcessing, prevFeaturePoints)    
    p = processing.MyProcessingFeaturePoints(featureFrameProcessing, vfps)
    p.fill_data()
    p.get_points_in_frames()
    p.get_y_values_of_point_in_frame(savePlot = False, plimit = 10)
    p.save_raw_feature_points('raw/phase3_feature_points')
    p.apply_cubic_spline_interpolation(savePlot= False, plimit = 10)
    p.normal_distribution_points_distance(savePlot= False)
    p.calculator_caculator_confidence_interval(percentage = 0.75, savePlot= False)
    p.validate_feature_points()
    p.save_plot_selected_point(plimit = 0)
    p.save_selected_points_to_txt("raw/phase3_selected_feature_points")
    
    
    fft_good_data_after_filtering = phase4_temporal_filtering(p.selected_validation_points)    
    pca_data = phase5_pca_data(fft_good_data_after_filtering)
    ####
    pca_data = np.asarray(pca_data)
    ######## Pulse Rate
    #pca_data = np.loadtxt("raw/phase5_pca_data")
    print '-- Phase 6: Selection Data and Pulse Rate'
    stop_time = datetime.datetime.now()
    print 'Total time process: %s' % (stop_time - start_time)
if __name__ == '__main__':
    main()