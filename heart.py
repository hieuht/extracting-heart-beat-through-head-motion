# -*- coding: utf-8 -*-
from scipy.fftpack import rfft, irfft
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import freqz
import numpy
import math    
import scipy
import  scipy.signal as signal
#########################
from scipy.signal import butter, lfilter
import mypeak

raw_data = np.loadtxt("raw/phase5_pca_data")
number_of_plot = len(raw_data)
Fs = 250.0 
T = 1.0 / Fs  #sample rate

f, axarr = plt.subplots(number_of_plot, 2)
for i, data in enumerate(raw_data):
    N = len(data)  #Number of samplepoints    
    t = np.linspace(0.0, N/ Fs, N, endpoint=True)
    axarr[i, 0].plot(t, data, 'b-')
    axarr[i, 0].grid(True)
    axarr[i, 0].set_xlabel("Times(s)")
    axarr[i, 0].set_ylabel("Total Energy")
    axarr[i, 0].set_ylabel("Total Energy")
    
    hann = np.hanning(len(data))
    Y = rfft(data * hann)
    dt = t[1] - t[0]
    fa = 1.0/dt # scan frequency
    freqs = np.linspace(0.0, fa / 2, N, endpoint=True)
    YF = np.abs(pow(2 *(Y[:N]) / N, 2))
    axarr[i, 1].plot(freqs, YF, 'b-')
    axarr[i, 1].grid(True)
    axarr[i, 1].set_xlabel("Frequency")
    axarr[i, 1].set_ylabel("Power") 
    # Find the peak in the coefficients
    idx = np.argmax(YF)
    freq = freqs[idx]
    v = freq / len(freqs)
    v = freq / len(freqs) * 1000 / 100
    if v <= 0.05 and v > 0:
        fpulse = float(abs(60.0 / freq))
        s = "%s Hz = %s bpm" % (freq, fpulse)
        print s        
        _max, _min = mypeak.peakdetect(data, t, Fs / fpulse, 1.0 / fpulse)
        xm = [p[0] for p in _max]    
        ym = [p[1] for p in _max]
        axarr[i, 0].plot(xm, ym, 'ro')
        a = {}
        if len(xm) > 0:            
            for py in xm:
                axarr[i, 0].plot(xm, ym, 'ro')    

plt.show()
plt.close()