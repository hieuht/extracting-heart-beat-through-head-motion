# -*- coding: utf-8 -*-
import Image
import cv2
import cv
import numpy as np
from scipy.spatial import distance
import matplotlib.pyplot as plt
import scipy.stats as stats
import pylab as pl
from scipy.interpolate import interp1d
import scipy as sp
import scipy
import threading
import sys
import datetime
class MyProcessingFeaturePoints:
    """"""

    #----------------------------------------------------------------------
    def __init__(self, feature_frame, vfps):
        """Constructor"""
        self.feature_frame = feature_frame
        self.points_in_frame = []
        self.filled_data = []
        self.max_points_in_frame = 0
        self.points_in_frame = []
        self.y_values = []
        self.points_after_cubic_spline = []
        self.mean = 0
        self.sigma = 0
        self.minR = 0
        self.maxR = 0
        self.pdf = 0
        self.maxDistanceList = []
        self.selected_validation_points = []
        self.vfps = vfps
    #----------------------------------------------------------------------
    def max_point(self):
        """Return the maximum number of point in list of frames"""
        tmp_store_max = []
        for frame in self.feature_frame:
            tmp_store_max.append(len(frame))
        self.max_points_in_frame = max(tmp_store_max)
        return max(tmp_store_max)
    #----------------------------------------------------------------------        
    def fill_data(self):
        """Hàm fill (0.0, 0.0) cho đủ dữ liệu các mảng thiếu giá trị"""
        frames = []
        for frame in self.feature_frame:
            frames.append(np.asarray(frame).tolist())        
        self.max_point()
        print '-- Max Tracked Feature Points in the frame: ', self.max_points_in_frame
        print '-- Filling 0.0 values if the frame missing feature points at the same position'
        frames_tmp = []
        for i, v in enumerate(frames):
            for j in xrange(self.max_points_in_frame-len(v)):
                v.append([0.0, 0.0])
            frames_tmp.append(v)	
        frames = []
        frames = np.array(frames_tmp)
        self.filled_data = frames
    #----------------------------------------------------------------------
    def get_points_in_frames(self):
        """
        Hàm trả về ds mảng, mỗi mảng thể hiện vị trí của 1 point theo tất cả frame [P*F]:
        P: Tổng số point
        F: Tổng số frame        
        """
        points = []
        for i in xrange(self.max_points_in_frame):
            points.append(self.filled_data[:,i,:].tolist())        
        self.points_in_frame = points
    #----------------------------------------------------------------------    
    def get_y_values_of_point_in_frame(self, savePlot = False, plimit = 20):
        """Get Y value of all points"""
        print '-- Get Yn(t) is used in our analysis'
        for i, frame in enumerate(self.points_in_frame):
            frame = np.asarray(frame)
            # Test
            if savePlot == True:
                if i < plimit:
                    print '---- Save plot point ' + str(i)
                    plt.plot(np.arange(0, len(frame.T[1])) / self.vfps, frame.T[1], 'bo')
                    plt.xlabel('Times (s)')
                    plt.ylabel('Y')                                
                    plt.savefig("plot/initPoints/point-"+str(i)+".jpg")
                    plt.close()
            self.y_values.append(frame.T[1])            
    #----------------------------------------------------------------------
    def apply_cubic_spline_interpolation(self, savePlot=False, plimit = 20):
        """
        Hàm áp dụng cubic spline interpolcation để tăng sample rate cho mỗi điểm lên 250Hz 
        (tần số của ECG = 250Hz)
        (tần số của máy ảnh điện thoại là 30Hz)
        Làm mịn dữ liệu
        """
        print '-- Apply Cubic Spline Interpolcation'
        points = np.asarray(self.y_values)
        total = len(points)
        print '---- Total Points: ' + str(total)
        start_time = datetime.datetime.now()        
        for i, y_point in enumerate(points):            
            y_point_len = len(y_point)
            y_point_times = np.arange(y_point_len, dtype=float)
            f = interp1d(y_point_times, y_point, kind='cubic')
            y_point_new_times = np.arange(0, y_point_len-1, 0.12)  #0.12=self.vfps/250 (eg: 30/250)
            y_point_new_times = y_point_new_times / self.vfps
            y_point_new = f(y_point_new_times)
            print '---- Point Number %s/%s: [%s -> %s]' % (i, total, y_point_len, len(y_point_new))
            self.points_after_cubic_spline.append(y_point_new)            
            if savePlot == True:
                if i < plimit:
                    self.thread_save_plot_point(i, y_point_new, y_point, y_point_new_times, y_point_times)
        stop_time = datetime.datetime.now()        
        #print stop_time - start_time

    #----------------------------------------------------------------------
    def thread_save_plot_point(self, i, y_point_new, y_point, y_point_new_times, y_point_times):        
        print '---- Saving Plot Point ' + str(i)
        plt.figure()
        #plt.plot(y_point_times, y_point, '-o', y_point_new_times, y_point_new, 'r-')
        plt.plot(y_point_new_times, y_point_new, 'ro')        
        plt.xlabel('Times (s)')
        plt.ylabel('Y')            
        plt.savefig("plot/interp1d/point-"+str(i)+".jpg")
        plt.close()        
    #----------------------------------------------------------------------    
    def normal_distribution_points_distance(self, savePlot=False):
        """Hàm trả về mean và sigma theo mô hình phân phối Gause"""
        print '-- Normal Distribution (Gause) Testing'
        self.max_distance_features_point_list(self.points_after_cubic_spline)
        print '-- Checking Normal Distribution (Gause) of Distance\'s Points'
        self.maxDistanceList.sort()
        self.sigma = np.std(self.maxDistanceList) 
        self.mean = np.mean(self.maxDistanceList)
        print '---- Mean', self.mean
        print '---- Sigma', self.sigma
        if (savePlot == True):
            self.thread_save_plot_normal_distribution_point()
    #----------------------------------------------------------------------
    def thread_save_plot_normal_distribution_point(self):
        plt.figure()
        self.pdf = stats.norm.pdf(self.maxDistanceList, self.mean, self.sigma)
        plt.title('Checking Normal Distribution (Gause) of Distance\'s Points')
        plt.plot(self.maxDistanceList, self.pdf)
        pl.hist(self.maxDistanceList,normed=True)
        plt.savefig("plot/normal_distribution_point.jpg")
        plt.close()
    #----------------------------------------------------------------------
    def max_distance_features_point_list(self, points):        
        """Hàm trả về danh sách khoảng cách lớn nhất của các điểm theo các frame"""
        for point in points:
            distancePoint = self.get_max_distance(point)            
            self.maxDistanceList.append(distancePoint)        
    #----------------------------------------------------------------------
    def calculator_caculator_confidence_interval(self, percentage, savePlot=False):
        """Hàm trả về khoảng giá trị confidence interval"""
        #http://stackoverflow.com/questions/15033511/compute-a-confidence-interval-from-sample-data
        self.minR, self.maxR = stats.norm.interval(percentage,loc=self.mean,scale= self.sigma)
        print '---- Value from an area (Use to compute Z for confidence intervals)'
        print '------ Between %s and %s' % (self.minR, self.maxR)
        if (savePlot == True):
            self.thread_save_plot_cofidence_interval()
    #----------------------------------------------------------------------
    def thread_save_plot_cofidence_interval(self):
        plt.figure()
        plt.title('Confidence Interval')
        plt.plot(self.maxDistanceList, self.pdf)
        pl.hist(self.maxDistanceList, normed=True)
        plt.axvline(self.minR)
        plt.axvline(self.maxR)
        plt.savefig("plot/cofidence_interval.jpg")        
        pl.close()
    #----------------------------------------------------------------------
    def validate_feature_points(self):
        """Hàm trả về các feature points đã được validate theo normal distriubution"""
        print '-- Selecting validate feature points'        
        for i, point in enumerate(self.points_after_cubic_spline):
            distancePoint = self.get_max_distance(point)
            if distancePoint > self.minR and distancePoint < self.maxR:
                self.selected_validation_points.append(point)
        print '---- Total Validate Feature Points %s/%s' % (len(self.selected_validation_points), len(self.points_after_cubic_spline))        
    #----------------------------------------------------------------------
    def save_plot_selected_point(self, plimit = 20):
        """"""
        for i, point in enumerate(self.selected_validation_points):
            if i < plimit:
                print '---- Plot selected point ' + str(i)
                plt.plot(np.arange(0, len(point)) / 250.0, point, 'go')
                plt.xlabel('Times')
                plt.ylabel('Y Dimension')            
                plt.savefig("plot/validationPoint/point-"+str(i)+".jpg")
                plt.close()                
    #----------------------------------------------------------------------
    def get_max_distance(self, inputPoints):
        """
        caculate a point's distance through all of frames
        Input: List of A(x,y) in each frame        
        """
        distances = []
        for i in xrange(len(inputPoints)-1):
            distance = inputPoints[i+1] - inputPoints[i]
            distances.append(distance)
        return max(distances)
    #----------------------------------------------------------------------
    def save_selected_points_to_txt(self, file_name):
        print '-- Save selected points to file %s' % file_name                
        x = np.asarray(self.selected_validation_points)
        np.savetxt(file_name, x)

    def save_raw_feature_points(self, file_name):
        print '-- Save y points to file %s' % file_name                
        x = np.asarray(self.y_values)
        np.savetxt(file_name, x)
        