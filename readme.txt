It is only a Prototype.
**Extracting heart beat through head motion from the video based on MIT Research. The project is my final year project in FPT University** 
*References: Balakrishnan, G., Durand, F. and Guttag, J. (2013). Detecting Pulse from Head Motions in Video. 2013 IEEE Conference on Computer Vision and Pattern Recognition.* 